pragma solidity ^0.5.0;

// C:/work/dfd/dfd/contracts/option.sol
// https://github.com/stanta/openzeppelin-contracts.gitvscode-solidity
// link to folder in project dir
import "./token/ERC20/ERC20Detailed.sol";
//import "./token/ERC20/ERC20Mintable.sol";
//import "../../access/roles/MinterRole.sol";

/*
1. Emitent emits ) option as smart contract:
BaseActive1,
BaseActive2,
OptionSumBA1sell
OptionSumBA2buy,
ExpireDAte
SecurityDepositPercent
NumberofTokens
1.1 Emitent pays  in BA1sell*SecurityDepositPercent =  security seposit  to contract as percent equal to sum of
 volatilities of Base Acives
2. Broker buy ERC20 tokens of emiting option as for same sum in BA2buy * SecurityDepositPercent
(percent equal to sum of volatilities of Base Acives).
3. At the day of option expires, Emitent should put residual part  of OptionsSum to the contract .
4. After that, broker can pay residue of sum for his/her tokens in BA2buy to contract and get proportional sum  of BA1sell
5. IF EMITENT escapes from paying residual sum of BA1sell, he/she lose SecurityDeposite and
5.1. Brokers have to send their option token to contract and
5.2  returns their broker's payments of SD + and get proportional part of  emitent's Security Deposite
as penalty for lose profit.
6. If allright, if emitent paied all sum of option to contract, after option expire,
emitent can withdraw unsold tokens and
 return un-exchanged part option.
*/
contract Options is ERC20Detailed {
    struct Option {
        address addrBA1sell; // address of ERC20 of  BaseActive1;
        address addrBA2buy;// address of ERC20 of BaseActive2;
        uint256 amountBA1sell; // amount of BaseActive1
        uint256 amountBA2buy; //  amount of BaseActive2
        uint256 sd1; // security deposite percent of BaseActive1
        uint256 sd2; // security deposite percent  of BaseActive2
        uint256 expSecs; //ExpireDays;
        string description;
        uint256 minusroyalty1000;
        uint256 isDeposited;
        uint256 fullDeposited;
   }

    Option  public thisOpt ;
    address private owner ;
    address private operator;
    
    mapping (address=>uint256) private purchases;
    mapping (address=>uint256) private options;
    address[] private listpurchases;
    uint256 private soldBA1;
    //ERC20 detailed
    string private _name;
    string private _symbol;
    uint8 private _decimals;
    uint256 internal _cap;
    uint256 private initPurshOptSum;
    
    constructor (string memory name, string memory symbol, uint8 decimals) public ERC20Detailed (name,  symbol,  decimals)  {
        
        _name = name;
        _symbol = symbol;
        _decimals = 18;
    }

    modifier onlyOwner() {
        require (owner == msg.sender, "Only owner can do this");
        _;
    }

    function setOwner (address _newOwner) public onlyOwner {
        owner = _newOwner;
    }
    
    // 1. Emitent emits ) option as smart contract:
    function setOption  (address _addrBA1sell, // address of ERC20 of  BaseActive1;
                address _addrBA2buy,// address of ERC20 of BaseActive2;
                uint256 _amountBA1sell, // sell
                uint256 _amountBA2buy, // buy
                uint256 _sd1,
                uint256 _sd2,
                uint256 _expSecs, //ExpireDays;
                string memory _description, // todo add name field
                uint256 _royalty1000,
                address _owner) 
    //            ERC20Capped (_amountBA1sell) 

        public  {
        require(_amountBA1sell > 0 && _amountBA2buy > 0 && _sd1 >0  && _sd2 > 0 && _expSecs > 0, "Parameters must be > 0");
        //to Wei
        thisOpt = Option (  _addrBA1sell, _addrBA2buy,
                            _amountBA1sell, _amountBA2buy,
                            _sd1, _sd2,
                            _expSecs, _description,
                            1000 - _royalty1000,
                            0,0);
        
        operator = msg.sender;
        owner = _owner;
        _cap = _amountBA1sell;
    }

    // 1.1 Emitent pays  in BA1sell*SecurityDepositPercent =  security seposit  to contract
    function makeDeposite () public   { //onlyOwner
        ERC20Detailed BA1sell = ERC20Detailed (thisOpt.addrBA1sell);
        uint256 sdValue =  thisOpt.sd1 * thisOpt.amountBA1sell  / 100 ;
        BA1sell.transferFrom(msg.sender, address(this), sdValue); 
        uint256 depBal = BA1sell.balanceOf(address(this));
        require (depBal > 0 && sdValue >0 && depBal >= sdValue,
                string(abi.encodePacked("Depositing failed, balanse: ", depBal, "but need: ", sdValue )));
        if (thisOpt.isDeposited == 0) {
            thisOpt.isDeposited = now;
        }
        if (thisOpt.sd1 >= 100 && thisOpt.fullDeposited == 0 ) { 
            thisOpt.fullDeposited = now;
        }
    }
/*
    function isHandMadeDeposite () public  { //onlyOwner
        ERC20Detailed  BA1sell = ERC20Detailed (thisOpt.addrBA1sell);
        if (BA1sell.balanceOf(address(this)) >= thisOpt.sd1 *  thisOpt.amountBA1sell / 100) {
            if (thisOpt.isDeposited == 0) {thisOpt.isDeposited = now;}
            if (thisOpt.sd1 >= 100 || thisOpt.fullDeposited == 0 ) { thisOpt.fullDeposited = now; }
        }

    } 
*/
    // 2. Broker buying  ERC20 tokens of emiting option as for same sum in BA2buy * SecurityDepositPercent
    function initPurshOpt (uint256 _amountbuyOpt) public { //buyOption

        require(thisOpt.isDeposited > 0, "No enought deposite allocated  emitent, wait a little.");
        require(thisOpt.isDeposited + thisOpt.expSecs  > now,
                "Too late to buy this options");
        ERC20Detailed BA2buy = ERC20Detailed (thisOpt.addrBA2buy);
        ERC20Detailed BA1sell = ERC20Detailed (thisOpt.addrBA1sell);
        uint256 BA2TransSum = _amountbuyOpt * thisOpt.sd2 * thisOpt.amountBA2buy / thisOpt.amountBA1sell / 100;
        BA2buy.transferFrom(msg.sender, address(this),BA2TransSum );
        mint(msg.sender, _amountbuyOpt);
        purchases[msg.sender] += BA2TransSum; //fact
        options [msg.sender] += _amountbuyOpt; //all
        listpurchases.push(msg.sender);
        initPurshOptSum += _amountbuyOpt;
    }
/*  todo: why for?
    function initPurshBA1sell (uint256 _amountBA1sell) public { //buyOption
        ERC20Detailed  BA2buy = ERC20Detailed (thisOpt.addrBA2buy);
        // uint256 depositedBA2buy = BA2buy.balanceOf(address(this));
        require(thisOpt.isDeposited > 0,
                "No enought deposite allocated  emitent, wait a little.");
        require(thisOpt.isDeposited + thisOpt.expSecs  > now,
                "Too late to buy this options");
        uint256 amountBA2buy = _amountBA1sell * thisOpt.amountBA2buy / thisOpt.amountBA1sell;
        // _incAllDlg(thisOpt.addrBA2buy, amountBA2buy * thisOpt.sd2 / 100); //now transfer only secure deposite

        //this.addMinter(msg.sender);
        uint256 optSum = _amountBA1sell;  // _amountBA2buy * thisOpt.amountBA1sell / thisOpt.amountBA2buy;
        mint(msg.sender, optSum);
        purchases[msg.sender] = amountBA2buy;
        options [msg.sender] = optSum;
        listpurchases.push(msg.sender);
    }
*/
    // 3. Before the day of option expires, Emitent should put residual part  of OptionsSum to the contract
    function finalFundOpt ()  public { //onlyOwner {
        ERC20Detailed BA1sell = ERC20Detailed (thisOpt.addrBA1sell);

        uint256 coefSD = thisOpt.sd1;
        if (coefSD < 100 ) {
            require(thisOpt.isDeposited + thisOpt.expSecs  > now,
                    "Too late to complete funding, option is broken");
            uint256 resOptValue = thisOpt.amountBA1sell * (100-coefSD) / 100;
            BA1sell.transferFrom(msg.sender, address(this), resOptValue); 
            uint256 depBal = BA1sell.balanceOf(address(this));
            require (depBal > 0 && resOptValue >0 && depBal >= resOptValue,
                string(abi.encodePacked("Depositing failed, balanse: ", depBal, "but need: ", resOptValue )));
        }
        thisOpt.fullDeposited = now;

    }

 //   4. After that, broker can pay residue of sum for his/her tokens in BA2buy to contract and get proportional sum  of BA1sell
    function finPursh(uint256 _amountbuyOpt)  public  { // if this broker get option from this contract
        require(thisOpt.isDeposited + thisOpt.expSecs  < now,
                string (abi.encodePacked("Too early to cashout, wait until option ripens for ",
                (thisOpt.isDeposited + thisOpt.expSecs - now) / 3600,
                " hours.")
                ));
        require(thisOpt.fullDeposited > 0, "This option is broken, use getPenalty(_optionsSum) function to withdraw");
        uint256 coefSD = thisOpt.sd2;
        if (coefSD < 100 ) {
        //4.1 get rest of BA2buy
            ERC20Detailed  BA2buy = ERC20Detailed (thisOpt.addrBA2buy);
            BA2buy.transferFrom(msg.sender, address(this), (100 - coefSD ) *  
            _amountbuyOpt * thisOpt.amountBA2buy / thisOpt.amountBA1sell / 100);
        }
        // 4.2 return opt tokens
        this.transferFrom(msg.sender, address(this),
                             _amountbuyOpt);
        //4.3 transfer BA1sell
        ERC20Detailed  BA1sell = ERC20Detailed (thisOpt.addrBA1sell);
        BA1sell.transfer(msg.sender, _amountbuyOpt * thisOpt.minusroyalty1000 / 1000);
        soldBA1 += _amountbuyOpt;

    }
/*
    function cashPartOption(uint256 _optionSumBA1sell)  public  { // if this broker get option from exchange or other way or partially
        require(thisOpt.isDeposited + thisOpt.expSecs  < now,
                "Too early to cashout, wait until option ripens");
        require(thisOpt.fullDeposited > 0, "This option is broken, use getPenalty(optionsSum) function to withdraw");
        //4.1 get rest of BA2buy
        uint256 amountBA2buy = _optionSumBA1sell * thisOpt.amountBA2buy / thisOpt.amountBA1sell;
        // _incAllDlg(thisOpt.addrBA2buy, amountBA2buy);

        // 4.2 return opt tokens
        this.transferFrom(msg.sender, address(this),
                             _optionSumBA1sell);
        //4.3 transfer BA1sell
        ERC20Detailed  BA1sell = ERC20Detailed (thisOpt.addrBA1sell);
        BA1sell.transfer(msg.sender, _optionSumBA1sell * thisOpt.minusroyalty1000 / 1000);
        soldBA1 += options[msg.sender];


    }
    */
/*
5.1. Brokers have to send their option token to contract and
5.2  returns their broker's payments of SD + and get proportional part of  emitent's Security Deposite
as penalty for lose profit.
*/
    function getPenalty (uint256 _optionSumBA1sell) public  {
        require(thisOpt.fullDeposited == 0 && now >  thisOpt.isDeposited + thisOpt.expSecs, "This option was fairly fullfilled by seller, make withdraw use finPursh() function.");
        // 5.1. return opt tokens
        this.transferFrom(msg.sender, address(this), _optionSumBA1sell);
        // 5.2.1. return proportional  BA2buy
        uint256 amountBA2buy = _optionSumBA1sell * thisOpt.amountBA2buy * thisOpt.sd2 / (thisOpt.amountBA1sell * 100);
        ERC20Detailed  BA2buy = ERC20Detailed (thisOpt.addrBA2buy);
        BA2buy.transfer(msg.sender, amountBA2buy);

        // _incAllDlg(thisOpt.addrBA2buy, amountBA2buy);
        // 5.2.2. return BA1sell penalti
        ERC20Detailed  BA1sell = ERC20Detailed (thisOpt.addrBA1sell);
        BA1sell.transfer(msg.sender, _optionSumBA1sell * thisOpt.sd1 / 100);

    }
/*
6. If allright, if emitent paied all sum of option to contract, after option expire,
emitent can withdraw BA2buy 
*/
    function withdrawSeller (uint256 _amountBA2buy) public onlyOwner {
        require(now > thisOpt.isDeposited + thisOpt.expSecs,
                "Too early to get back, wait until option ripens");
        require(thisOpt.fullDeposited > 0, "Sorry but no!. This option wasn't fullfill by seller!");
        ERC20Detailed  BA2buy = ERC20Detailed (thisOpt.addrBA2buy);
        uint256 amountBA2buy =  _amountBA2buy * thisOpt.minusroyalty1000 / 1000;
        BA2buy.transfer(owner,amountBA2buy);
    }
/*
6.1. and  unsold tokens and
 return un-exchanged part option.
*/
    
    function returnUnsoldDepositBA1sell () public onlyOwner {
        require(thisOpt.isDeposited + thisOpt.expSecs  < now,
                "Too early to get back, wait until option matures");
        require(thisOpt.fullDeposited > 0, "Sorry but you didn't fullfill the option!");
        ERC20Detailed  BA1sell = ERC20Detailed (thisOpt.addrBA1sell);
        BA1sell.transfer(msg.sender, (thisOpt.amountBA1sell - soldBA1) * thisOpt.minusroyalty1000 / 1000);

    }
    //todo 
    /** what if option buyer whan't not cash his options? Forget about it? Loose private key?
    6.2 --
     */
    

/*
7.1 Operator can get royalty
*/ 
    function getRoyalty () public {
        require(thisOpt.isDeposited + thisOpt.expSecs  < now,
                "Too early to get back, wait until option matures");
        ERC20Detailed  BA1sell = ERC20Detailed (thisOpt.addrBA1sell);
        ERC20Detailed  BA2buy = ERC20Detailed (thisOpt.addrBA2buy);
        BA1sell.transfer(operator, thisOpt.amountBA1sell * (1000 - thisOpt.minusroyalty1000) / 1000);
        uint256 amountBA2buy = soldBA1 * thisOpt.amountBA2buy / thisOpt.amountBA1sell *
                                (1000 - thisOpt.minusroyalty1000) / 1000;
        BA2buy.transfer(operator,amountBA2buy);

    }
        /**
    7.2 after options mature finished, operaator get rest of BA1sell from for locked unfunded (brocken) deposite of BA2buy
     */

    function getOperatorPenalty () public {
        require(thisOpt.isDeposited + thisOpt.expSecs  < now && thisOpt.fullDeposited == 0  , 
                "Use this function just for broken options after exping date");
        ERC20Detailed  BA1sell = ERC20Detailed (thisOpt.addrBA1sell);
        ERC20Detailed  BA2buy = ERC20Detailed (thisOpt.addrBA2buy);
        BA1sell.transfer(operator, (thisOpt.amountBA1sell - initPurshOptSum) * thisOpt.sd1);

    }
    
    //8 . todo add stat functions 

    
        /**
     * @dev See {ERC20-mint}.
     *
     * Requirements:
     *
     * - `value` must not cause the total supply to go over the cap.
     */
    function mint(address account, uint256 value) internal {
        require(totalSupply().add(value) <= _cap, "ERC20Capped: cap exceeded");
        super._mint(account, value);
    }

    /**
     * @dev Returns the cap on the token's total supply.
     */
    function cap() public view returns (uint256) {
        return _cap;
    }
    
}