pragma solidity ^0.5.0;
import "token/ERC20/ERC20.sol";
import "token/ERC20/ERC20Detailed.sol";

contract wETHtst is ERC20, ERC20Detailed {

  constructor () 
        ERC20Detailed("wETHtest",  //name
              "WET", //symbol
              18 )//decimals
        ERC20()          
         public {
      _mint(msg.sender, 1000000 * 10**uint(decimals()));
    }

}