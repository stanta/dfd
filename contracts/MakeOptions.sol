pragma solidity ^0.5.0;

import "./Options.sol";


contract MakeOptions {
    //address[] public optionList;
    mapping (address => address[]) private  optionList; // options list for different users
    address private owner ;
    
    function setOwner (address _newOwner) public onlyOwner {
        owner = _newOwner;
    }

    constructor () public {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require (owner == msg.sender, "Only owner can do this");
        _;
    }

    function makeOption(address _addrBA1, // address of ERC20 of  BaseActive1;
                        address _addrBA2,// address of ERC20 of BaseActive2;
                        uint256 _amountBA1, //  security deposite percent of BaseActive1
                        uint256 _amountBA2, //  security deposite percent of BaseActive1
                        uint256 _sd1, //  security deposite percent of BaseActive1
                        uint256 _sd2, //  security deposite percent of BaseActive1
                        uint256 _expSecs, //ExpireDays;
                        string  memory _description                  
                        ) public  {
        uint256 royalty1000 = 10; // 1%
        string memory _name = string( (abi.encodePacked("0xy: ", _description)));
        string memory _symbol = "0xyO";
        uint8 _decimals = 18;

        Options newOption = new Options(_name,_symbol, _decimals);
        
        newOption.setOption( _addrBA1, _addrBA2,
                     _amountBA1, _amountBA2,
                     _sd1, _sd2,
                     _expSecs, _description,
                      royalty1000, msg.sender);
        optionList[msg.sender].push(address(newOption));
        return;
        }
        function getLast(address _owner) public view returns (address) {
            return optionList[_owner][optionList[_owner].length -1];
        }

        function getOptionsList (address _owner) public view returns (address[] memory) {
            return optionList[_owner];
        }

        function getOperatorPenalty (address _brokenOption)  public  onlyOwner  {
            Options brockOpt = Options (_brokenOption);
            brockOpt.getOperatorPenalty ();

        }

        function withdraw (address _baseActive, uint256 _amount) public  onlyOwner {
            ERC20Detailed  BA = ERC20Detailed (_baseActive);
            BA.transfer (owner, _amount);
        }

    }
