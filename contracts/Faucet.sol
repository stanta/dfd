pragma solidity ^0.5.0;
import "./USDTtst.sol";
import "./wETHtst.sol";

contract Faucet {

USDTtst tokUSDTtst;
wETHtst tokwETHtst;

mapping (address => uint256) listTakers;

    constructor () public {
       
        tokUSDTtst = new USDTtst();
        tokwETHtst = new wETHtst();

    }

    function sendTokens () public  {
        require (listTakers[msg.sender] == 0 || listTakers[msg.sender] + 600 < now,
         "You can request just once per 10 min");
            tokwETHtst.transfer(msg.sender, 10*10 ** uint(tokwETHtst.decimals()));
            tokUSDTtst.transfer(msg.sender, 10*10 ** uint(tokUSDTtst.decimals()));
            listTakers[msg.sender] = now; 
        
    }
        
    function getAddresses () public view  returns  (address[2] memory){
        address[2] memory tokens;
        tokens[0] = address (tokUSDTtst);
        tokens[1] = address (tokwETHtst);
        return tokens;
    }

}