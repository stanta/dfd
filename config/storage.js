module.exports = {
  // default applies to all environments
  default: {
    enabled: false,
    ipfs_bin: "ipfs",
    available_providers: ["ipfs"],
    upload: {
      provider: "ipfs",
      host: "localhost",
      port: 5001
    },
    dappConnection: [
      {
        provider:"ipfs",
        host: "localhost",
        port: 5001,
        getUrl: "http://localhost:8080/ipfs/"
      }
    ]
    // Configuration to start Swarm in the same terminal as `embark run`
    /*,account: {
      address: "YOUR_ACCOUNT_ADDRESS", // Address of account accessing Swarm
      password: "PATH/TO/PASSWORD/FILE" // File containing the password of the account
    },
    swarmPath: "PATH/TO/SWARM/EXECUTABLE" // Path to swarm executable (default: swarm)*/
  },

  // default environment, merges with the settings in default
  // assumed to be the intended environment by `embark run`
  development: {
    upload: {
      provider: "ipfs",
      host: "localhost",
      port: 5001,
      getUrl: "http://localhost:8080/ipfs/"
    }
  },

  // merges with the settings in default
  // used with "embark run privatenet"
  privatenet: {
  },

  // merges with the settings in default
  // used with "embark run testnet"
  testnet: {
  },

  // merges with the settings in default
  // used with "embark run livenet"
  livenet: {
  },

  // you can name an environment with specific settings and then specify with
  // "embark run custom_name"
  //custom_name: {
  //}
  externalnode: {
    "enabled": true,
    "upload":{
      "provider": "ipfs",
      "host": "ipfs.infura.io/ipfs/Qmaisz6NMhDB51cCvNWa1GMS7LU1pAxdF4Ld6Ft9kZEP2a",
      "port": 80,
      "protocol": "https",
      "getUrl": "https://ipfs.infura.io/ipfs/"
    }
    
  /*  provider: "ftp",
    host: "ftp://u0807850_0xytech:0Z5a0F8y@0xytech.com/simpleoption/",
    port:20 */
  },
};
