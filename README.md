# `embark-dapp-template-demo`

> Demo DApp Simple delivery option for Embark

www.0xy.tech

Visit [embark.status.im](https://embark.status.im/) to get started with
[Embark](https://github.com/embark-framework/embark).
