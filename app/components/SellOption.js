import EmbarkJS from 'Embark/EmbarkJS';

import React from 'react';
import {Form, FormGroup, Input, HelpBlock, Button, FormText} from 'reactstrap';

import ERC20 from '../../embarkArtifacts/contracts/ERC20Detailed';
import SimpleStorage from '../../embarkArtifacts/contracts/SimpleStorage';
import MakeOptions from '../../embarkArtifacts/contracts/MakeOptions';
import Options from '../../embarkArtifacts/contracts/Options';
//import ERC20 from '../../embarkArtifacts/contracts/ERC20';
import List from 'react-list-select'
import ReactGA from 'react-ga';
ReactGA.initialize('UA-161540415-1');
ReactGA.pageview(window.location.pathname + window.location.search);

class SellOption extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      valueSet: 10,
      getValue: "",
      logs: [],
      name:"",
      addrBA1sell: "",
      addrBA2buy: "",
      amountBA1sell: "",
      amountBA2buy: "",
      symbolBA1: "",
      symbolBA2: "",
      decimalsBA1: 18,
      decimalsBA2: 18,
      sd1: 10,
      sd2: 10,
      expSecs: 36000,
      description: "test",
      optionsList: [],
      isDeposited: 0,
      fullDeposited: 0,
      curOption: "",
      curOptstate:"" ,
      ERC20: "" ,
      sumToWithdrawSel: 0
    };
    
  }

  
  handleChange(e) {
    let keyVal = {}
    keyVal[e.target.name] = e.target.value;
    this.setState( keyVal );
                 
  }

    
  handleChangeList(e) {
    let keyVal = {}
    keyVal["getValue"] = this.state.optionsList[e];
    this.setState( keyVal );
                 
  }

  checkEnter(e, func) {
    if (e.key !== 'Enter') {
      return;
    }
    e.preventDefault();
    func.apply(this, [e]);
  }


  _addToLog(txt) {
    this.state.logs.push(txt);
    this.setState({ logs: this.state.logs });
  }

  async deployOption (e) {
    
    e.preventDefault();
    await EmbarkJS.enableEthereum();
    const ERC20BA1sell =  EmbarkJS.Blockchain.Contract({
      abi: ERC20.options.jsonInterface,
      address: this.state.addrBA1sell
      });
    const ERC20BA2Buy  =  EmbarkJS.Blockchain.Contract({
        abi: ERC20.options.jsonInterface,
        address: this.state.addrBA2buy
        });

    this.state.decimalsBA1 = await  ERC20BA1sell.methods.decimals().call();
    this.state.decimalsBA2 = await  ERC20BA2Buy.methods.decimals().call();
//web3.utils.toBN
    
    let amountBA1sell =  web3.utils.toBN(web3.utils.toWei (this.state.amountBA1sell));
    let decimalsBA1 =  web3.utils.toBN(10**(this.state.decimalsBA1 - 18))
    amountBA1sell =   amountBA1sell.mul(decimalsBA1);
    let  amountBA2buy =  web3.utils.toBN(web3.utils.toWei (this.state.amountBA2buy));
    let decimalsBA2 =  web3.utils.toBN(10**(this.state.decimalsBA2 - 18))
    amountBA2buy = amountBA2buy.mul(decimalsBA2);
    let expSecs = this.state.expDays*86400 + this.state.expHours *3600;

    MakeOptions.methods.makeOption(
                 this.state.addrBA1sell,
                 this.state.addrBA2buy,
                 amountBA1sell.toString(),
                 amountBA2buy.toString(),
                 parseInt(this.state.sd1, 10),
                 parseInt(this.state.sd2, 10),
                 expSecs,
                 this.state.description
                ).send();
    this._addToLog("MakeOptions.methods.MakeOptions: ", this.state.getValue);

  }

  async getOptList(e) {
    e.preventDefault();
    await EmbarkJS.enableEthereum();
    let  account;
    await web3.eth.getAccounts().then(e => { account = e[0];  
      });
    MakeOptions.methods.getOptionsList(account).call().then(_value => this.setState({ optionsList: _value }));
    
  }


  async getValue(e) {
    e.preventDefault();
    await EmbarkJS.enableEthereum();
    let  account;
    await web3.eth.getAccounts().then(e => { account = e[0];  
      });
    MakeOptions.methods.getLast(account).call().then(_value => this.setState({ getValue: _value }));
    
    this.state.curOption =  EmbarkJS.Blockchain.Contract({
        abi: Options.options.jsonInterface,
        address: this.state.getValue});
    
    await this.state.curOption.methods.thisOpt().call().then(_value =>
      {
      const paramsToString = params => Object.entries(params).reduce((acc, [key, value], index, array) => `${acc}${key}=${encodeURIComponent(value)}${index !== (array.length - 1) ? '&' : ''}`, "");    
      this.setState({addrBA1sell: _value.addrBA1sell});
      this.setState({addrBA2buy: _value.addrBA2buy});
      this.setState({amountBA1sell: _value.amountBA1sell});
      this.setState({amountBA2buy: _value.amountBA2buy});
      this.setState({sd1: _value.sd1});
      this.setState({sd2: _value.sd2});
      this.setState({expSecs: _value.expSecs});
      this.setState({description: _value.description});
      this.setState({isDeposited: _value.isDeposited});
      this.setState({fullDeposited: _value.fullDeposited});
      this.setState({description: _value.description});
      this.setState({ curOptstate: paramsToString(_value) })
      });
      this.state.name = await this.state.curOption.methods.name().call()
      const ERC20BA1sell =  EmbarkJS.Blockchain.Contract({
        abi: ERC20.options.jsonInterface,
        address: this.state.addrBA1sell
        });
      const ERC20BA2Buy  =  EmbarkJS.Blockchain.Contract({
          abi: ERC20.options.jsonInterface,
          address: this.state.addrBA2buy
          });
  
      this.state.decimalsBA1 = await  ERC20BA1sell.methods.decimals().call();
      this.state.symbolBA1 = await  ERC20BA1sell.methods.symbol().call();
      this.state.decimalsBA2 = await  ERC20BA2Buy.methods.decimals().call();
      this.state.symbolBA2 = await  ERC20BA2Buy.methods.symbol().call();
    this._addToLog("Option address: ", this.state.getValue );
  }

    
  async registerDeposite(e) {
    e.preventDefault();
    await EmbarkJS.enableEthereum();
    this.state.curOption =  EmbarkJS.Blockchain.Contract({
                                    abi: Options.options.jsonInterface,
                                    address: this.state.getValue});

    this.state.curOption.methods.isHandMadeDeposite().send(); //({gas: gasAmount});

    this._addToLog("Option deposited time: ", this.state.isDeposited);
  }

  async approveDep(e) {
    
    await EmbarkJS.enableEthereum();
    try {

      this.state.ERC20 =  EmbarkJS.Blockchain.Contract({
      abi: ERC20.options.jsonInterface,
      address: this.state.addrBA1sell
      });
      
      //const decimals = await this.state.ERC20.methods.decimals().call();
      const amountSD = web3.utils.toBN( this.state.sd1 * this.state. amountBA1sell / 100 );
  
      this.state.ERC20.methods.approve(this.state.getValue, amountSD.toString()).send();
   }
   catch (err) {
     console.log (err);
   }
  }

  async makeDeposite(e) {    
    await EmbarkJS.enableEthereum();
    try {
      this.state.curOption =  EmbarkJS.Blockchain.Contract({
        abi: Options.options.jsonInterface,
        address: this.state.getValue
      });
      
      this.state.curOption.methods.makeDeposite().send();
   }
   catch (err) {
     console.log (err);
   }
  }
  

  async approveFull(e) {
    
    await EmbarkJS.enableEthereum();
    try {

      this.state.ERC20 =  EmbarkJS.Blockchain.Contract({
      abi: ERC20.options.jsonInterface,
      address: this.state.addrBA1sell
      });
      
      //const decimals = await this.state.ERC20.methods.decimals().call();
      const amountSD = web3.utils.toBN( (100- this.state.sd1) * this.state.
        amountBA1sell / 100 );
  
      this.state.ERC20.methods.approve(this.state.getValue, amountSD.toString()).send();
   }
   catch (err) {
     console.log (err);
   }
  }

  async finalFundOpt(e) {    
    await EmbarkJS.enableEthereum();
    try {
      this.state.curOption =  EmbarkJS.Blockchain.Contract({
        abi: Options.options.jsonInterface,
        address: this.state.getValue
      });
      
      this.state.curOption.methods.finalFundOpt().send();
   }
   catch (err) {
     console.log (err);
   }
  }
  
  async withdrawSeller(e) {    
    await EmbarkJS.enableEthereum();
    try {
      this.state.curOption =  EmbarkJS.Blockchain.Contract({
        abi: Options.options.jsonInterface,
        address: this.state.getValue
      });
      let amntWthdrSelBN = web3.utils.toWei (this.state.sumToWithdrawSel) ;
    
      this.state.curOption.methods.withdrawSeller(amntWthdrSelBN.toString()).send();
   }
   catch (err) {
     console.log (err);
   }
  }

  render() {
    return (<React.Fragment>
        
        
        <h3> 1. Get the current option address value</h3>
        <Form>
          <FormGroup>
            <Button color="primary" onClick={(e) => this.getOptList(e)}>Get my options list</Button>
            <List
                items={this.state.optionsList}
            //  selected={[0]}
            //    disabled={[4]}
                multiple={false}
          //      onClick={(selected) => {this.state.getValue = _this.props.children }}
                onChange={(e) => this.handleChangeList(e)}/>
                
            <FormText color="muted">Or paste option Smart contract address </FormText>
                  <Input type = "text"
                    key="getValue"
                // initialValues  = {this.state.getValue}
                    name="getValue"
                    placeholder="Ethereum smart contract address 0x..."
                   // initial// initialValues  = {this.state.getValue}
                    onChange={(e) => this.handleChange(e)}/>
            <p>Current address value is <span className="value font-weight-bold">{this.state.getValue}</span></p>
            {this.state.getValue && this.state.getValue !== 0 &&
            <Button color="primary" onClick={(e) => this.getValue(e)}>Get full option data</Button>
            }
            <FormText color="muted">Click the button to get the option address value.</FormText>
            {this.state.getValue && this.state.getValue !== 0 &&
            <p>Current option is at  <span className="value font-weight-bold">{this.state.getValue}</span> <br />
            <br/>
           Description: {this.state.description}
           <br /> 
            ETH Address active to sell: {this.state.addrBA1sell } <br/>
            ETH symbol active to sell: {this.state.symbolBA1 } <br/>
            Amount active to sell: {this.state.amountBA1sell / 10**(this.state.decimalsBA1)} <br/>
            Depositing active to sell: {this.state.sd1 } % <br/>
            <br/>
            Address active  to buy: {this.state.addrBA2buy  } <br/>
            ETH symbol active to buy: {this.state.symbolBA2 } <br/>
            Amount active to buy: {this.state.amountBA2buy / 10**(this.state.decimalsBA2) } <br/>
            Depositing active to buy: {this.state.sd2 } % <br/>
            <br/>
            Deposited at: { (this.state.isDeposited > 0) ? (new Date(this.state.isDeposited * 1000)).toString()  : "still not" } <br/>
            Expired at: { (this.state.isDeposited > 0) ? (new Date((parseInt(this.state.isDeposited) + parseInt(this.state.expSecs)) * 1000)).toString() : "still no deposite" } <br/>
            FullDeposited at: { (this.state.fullDeposited > 0) ? (new Date(this.state.fullDeposited*1000 )).toString() : "still not"} <br/>
            <br /> 
            </p>}
          </FormGroup>
        </Form>
        
        <h3> 2. Make secure deposite </h3>
        {this.state.isDeposited == 0 &&
        <Form>
          <FormGroup>
            <FormText >Please first approve transfer to option smart contract {this.state.getValue} your secure deposite as {this.state.sd1}% of {this.state.amountBA1sell / 10**(this.state.decimalsBA1)} = {this.state.sd1 * this.state.amountBA1sell / 100 / 10**(this.state.decimalsBA1)}  {this.state.symbolBA1} tokens</FormText>
            <br/>
            
            <Button color="primary" onClick={(e) => this.approveDep(e)}>
        Approve transferFrom {this.state.sd1 * this.state.amountBA1sell / 100 / 10**this.state.decimalsBA1} {this.state.symbolBA1} tokens
            </Button>
            <FormText color="muted">Click the button make deposite.</FormText>

              <br/>
              
            <Button color="primary" onClick={(e) => this.makeDeposite(e)}>Make Deposite</Button>
            <FormText color="muted">Click the button make deposite.</FormText>
              
          </FormGroup>
        </Form>
        }
      <h3> 3.Make full deposite  </h3>
      {this.state.fullDeposited == 0 &&
        <Form>
          
        <FormGroup>
        <FormText >Please first approve transfer to option smart contract {this.state.getValue} your secure deposite as {100 - this.state.sd1}% of {this.state.amountBA1sell / 10**(this.state.decimalsBA1 )} = {(100 -this.state.sd1) * this.state.amountBA1sell / 100 / 10**(this.state.decimalsBA1 )} {this.state.symbolBA1} tokens</FormText>
            <br/>
 
          <FormText color="muted">Click the button make full  deposite.</FormText>
        {this.state.isDeposited && this.state.isDeposited !== 0 &&
           <Button color="primary" onClick={(e) => this.approveFull(e)}>
           Approve transferFrom {(100 - this.state.sd1) * this.state.amountBA1sell / 100 / 10**this.state.decimalsBA1} tokens
         </Button>
        }      
          <FormText color="muted">Click the button make FULL deposite.</FormText>

            <br/>
            {this.state.isDeposited && this.state.isDeposited !== 0 &&
          <Button color="primary" onClick={(e) => this.finalFundOpt(e)}>Make Deposite</Button>
          }
          <FormText color="muted">Click the button make FULL  deposite.</FormText>
            

        </FormGroup>
        </Form>
        }
        <h3> 4. Withdraw sold </h3>
          <Form>
          <FormGroup>               
              <p>Current address value is <span className="value font-weight-bold">{this.state.getValue}</span></p>
              <Input type = "number"
                      step={'.0001'}
                      key="sumToWithdrawSel"
                      // initialValues  = {this.state.amountBA1sell}
                          name="sumToWithdrawSel"
                          placeholder="Sum in BUY tokens you want to withdraw"                  
                      onChange={(e) => this.handleChange(e)}/>   

             
              <Button color="primary" onClick={(e) => this.withdrawSeller(e)}>
                Withdraw {this.state.sumToWithdrawSel} {this.state.symbolBA2} </Button>
              <FormText color="muted">Click the button make withdraw.</FormText>

              </FormGroup>

              
          </Form>
        <h3> Logs </h3>
        <p> calls being made: </p>
        <div className="logs">
          {
            this.state.logs.map((item, i) => <p key={i}>{item}</p>)
          }
        </div>
      </React.Fragment>
    );
  }
}

export default SellOption;
