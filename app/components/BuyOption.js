import EmbarkJS from 'Embark/EmbarkJS';

import React from 'react';
import {Form, FormGroup, Input, HelpBlock, Button, FormText} from 'reactstrap';

import ERC20 from '../../embarkArtifacts/contracts/ERC20Detailed';
import SimpleStorage from '../../embarkArtifacts/contracts/SimpleStorage';
import MakeOption from '../../embarkArtifacts/contracts/MakeOption';
import Options from '../../embarkArtifacts/contracts/Options';
//import ERC20 from '../../embarkArtifacts/contracts/ERC20';
import ReactGA from 'react-ga';
ReactGA.initialize('UA-161540415-1');
ReactGA.pageview(window.location.pathname + window.location.search);

class BuyOption extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      valueSet: 10,
      getValue: "",
      logs: [],
      addrBA1sell: 0x0000000000000000000000,
      addrBA2buy: 0x0000000000000000000000,
      amountBA1sell: 1000,
      amountBA2buy: 1000,
      sd1: 10,
      sd2: 10,
      expDays: 30,
      voidDays: 30,
      description: "",
      optionsList: [],
      isDeposited: 0,
      fullDeposited: 0,
      curOption: "",
      curOptstate:"" ,
      ERC20: "" ,
      sumToPurshase: 0,
      amounttoTransAppr:0, 
      sumToWithdraw:0,
      
    };
  }

  handleChange(e) {
    let keyVal = {}
    keyVal[e.target.name] = e.target.value;
    this.setState( keyVal );
                 
  }

  checkEnter(e, func) {
    if (e.key !== 'Enter') {
      return;
    }
    e.preventDefault();
    func.apply(this, [e]);
  }


  _addToLog(txt) {
    this.state.logs.push(txt);
    this.setState({ logs: this.state.logs });
  }


  async getValue(e) {
    e.preventDefault();
    await EmbarkJS.enableEthereum();
    let  account;
    await web3.eth.getAccounts().then(e => { account = e[0];  
      });
    MakeOption.methods.getLast(account).call().then(_value => this.setState({ getValue: _value }));
    
    this.state.curOption =  EmbarkJS.Blockchain.Contract({
        abi: Options.options.jsonInterface,
        address: this.state.getValue});
    
    await this.state.curOption.methods.thisOpt().call().then(_value =>
      {
      const paramsToString = params => Object.entries(params).reduce((acc, [key, value], index, array) => `${acc}${key}=${encodeURIComponent(value)}${index !== (array.length - 1) ? '&' : ''}`, "");    
      this.setState({addrBA1sell: _value.addrBA1sell});
      this.setState({addrBA2buy: _value.addrBA2buy});
      this.setState({amountBA1sell: _value.amountBA1sell});
      this.setState({amountBA2buy: _value.amountBA2buy});
      this.setState({sd1: _value.sd1});
      this.setState({sd2: _value.sd2});
      this.setState({expDays: _value.expDays});
      this.setState({description: _value.description});
      this.setState({isDeposited: _value.isDeposited});
      this.setState({fullDeposited: _value.fullDeposited});
      this.setState({ curOptstate: paramsToString(_value) })
      });
      this.state.name = await this.state.curOption.methods.name().call()
      const ERC20BA1sell =  EmbarkJS.Blockchain.Contract({
        abi: ERC20.options.jsonInterface,
        address: this.state.addrBA1sell
        });
      const ERC20BA2Buy  =  EmbarkJS.Blockchain.Contract({
          abi: ERC20.options.jsonInterface,
          address: this.state.addrBA2buy
          });
  
      this.state.decimalsBA1 = await  ERC20BA1sell.methods.decimals().call();
      this.state.symbolBA1 = await  ERC20BA1sell.methods.symbol().call();
      this.state.decimalsBA2 = await  ERC20BA2Buy.methods.decimals().call();
      this.state.symbolBA2 = await  ERC20BA2Buy.methods.symbol().call();
    this._addToLog("Option address: ", this.state.getValue );
  }


  async approveSD(e) {
    
    await EmbarkJS.enableEthereum();
    try {
      this.state.ERC20 =  EmbarkJS.Blockchain.Contract({
        abi: ERC20.options.jsonInterface,
        address: this.state.addrBA2buy
      });

      const decimals = await this.state.ERC20.methods.decimals().call();
      
      let amounttoTransAppr =this.state.sumToPurshase * this.state.sd2 * this.state.amountBA2buy / this.state.amountBA1sell * 10**(decimals - 18)/100;
      let amounttoTransApprBN = web3.utils.toWei(amounttoTransAppr.toString());
/*
      const price = web3.utils.toBN(this.state.amountBA2buy).div(web3.utils.toBN(this.state.amountBA1sell ));

      let pricedecimalsBuy =  price.mul(web3.utils.toBN(10**(decimals - 18)));
      let amounttoTransApprBN =  web3.utils.toBN(web3.utils.toWei (amounttoTransAppr.toString()));
      amounttoTransApprBN = amounttoTransApprBN.mul(pricedecimalsBuy).div(web3.utils.toBN(1000000));
      */
      this.state.ERC20.methods.approve(this.state.getValue, amounttoTransApprBN).send();
   }
   catch (err) {
     console.log (err);
   }
  }

  async initPurshOpt(e) {    
    await EmbarkJS.enableEthereum();
    try {
      this.state.curOption =  EmbarkJS.Blockchain.Contract({
        abi: Options.options.jsonInterface,
        address: this.state.getValue
      });
;
      
     
      let amounttoTransApprBN =  web3.utils.toWei (this.state.sumToPurshase );
      
      this.state.curOption.methods.initPurshOpt(amounttoTransApprBN).send();
   }
   catch (err) {
     console.log (err);
   }
  }

  async approveOpt(e) {
    
    await EmbarkJS.enableEthereum();
    try {
      //approving sent options tokens
      this.state.curOption =  EmbarkJS.Blockchain.Contract({
        abi: Options.options.jsonInterface,
        address: this.state.getValue
      });
      let amntOptWithdrBN = web3.utils.toWei ((this.state.sumToWithdraw));
      this.state.curOption.methods.approve(this.state.getValue, amntOptWithdrBN.toString()).send();
    

   }
   catch (err) {
     console.log (err);
   }
  }

  async approveWithdr(e) {
    
    await EmbarkJS.enableEthereum();
    try {
      //approving fullfilling of BA2 tokens
      this.state.ERC20 =  EmbarkJS.Blockchain.Contract({
        abi: ERC20.options.jsonInterface,
        address: this.state.addrBA2buy
      });
      const decimals = await this.state.ERC20.methods.decimals().call();
      let amontBA2tofullfillAppr =this.state.sumToWithdraw * (100 - this.state.sd2 ) * 10000;
      const price = web3.utils.toBN(this.state.amountBA2buy).div(web3.utils.toBN(this.state.amountBA1sell ));
      let pricedecimalsBuy =  price.mul(web3.utils.toBN(10**(decimals - 18)));
      let amontBA2tofullfillApprBN =  web3.utils.toWei (web3.utils.toBN(amontBA2tofullfillAppr));
      amontBA2tofullfillApprBN = amontBA2tofullfillApprBN.mul(pricedecimalsBuy).div(web3.utils.toBN(1000000));
      this.state.ERC20.methods.approve(this.state.getValue, amontBA2tofullfillApprBN.toString()).send();


   }
   catch (err) {
     console.log (err);
   }
  }

  async finPurshBA2buy(e) {    
    await EmbarkJS.enableEthereum();
    try {
      this.state.curOption =  EmbarkJS.Blockchain.Contract({
        abi: Options.options.jsonInterface,
        address: this.state.getValue
      });
      let amntOptWithdrBN = web3.utils.toWei ((this.state.sumToWithdraw));
    
      this.state.curOption.methods.finPursh(amntOptWithdrBN).send();
   }
   catch (err) {
     console.log (err);
   }
  }
  
  async getPenalty(e) {    
    await EmbarkJS.enableEthereum();
    try {
      this.state.curOption =  EmbarkJS.Blockchain.Contract({
        abi: Options.options.jsonInterface,
        address: this.state.getValue
      });
      let amntOptPenBN = web3.utils.toWei ((this.state.sumToWithdraw));
    
      this.state.curOption.methods.getPenalty(amntOptPenBN.toString()).send();
   }
   catch (err) {
     console.log (err);
   }
  }

  render() {
    return (<React.Fragment>
        
     <h3>  Get the current option address value</h3>
        <Form>
          <FormGroup>
          
            <FormText color="muted">Paste Smart contract address of option token from ETH wallet</FormText>
                  <Input type = "text"
                    key="getValue"
                // initialValues  = {this.state.getValue}
                    name="getValue"
                    placeholder="Ethereum smart contract address 0x..."
                   // initial// initialValues  = {this.state.getValue}
                    onChange={(e) => this.handleChange(e)}/>     
            <Button color="primary" onClick={(e) => this.getValue(e)}>Get option data </Button>          
            <FormText color="muted">Click the button to get the option address value.</FormText>

            {this.state.getValue && this.state.getValue !== 0 &&
           <p>Current option is at  <span className="value font-weight-bold">{this.state.getValue}</span> <br />
            <br/>
           Description: {this.state.description}
           <br /> 
           ETH Address active to sell: {this.state.addrBA1sell } <br/>
           ETH symbol active to sell: {this.state.symbolBA1 } <br/>
           Amount active to sell: {this.state.amountBA1sell / 10**(this.state.decimalsBA1)} <br/>
           Depositing active to sell: {this.state.sd1 } % <br/>
           <br/>
           Address active  to buy: {this.state.addrBA2buy  } <br/>
           ETH symbol active to buy: {this.state.symbolBA2 } <br/>
           Amount active to buy: {this.state.amountBA2buy / 10**(this.state.decimalsBA2) } <br/>
           Depositing active to buy: {this.state.sd2 } % <br/>
           <br/>
           Deposited at: { (this.state.isDeposited > 0) ? (new Date(this.state.isDeposited * 1000)).toString()  : "still not" } <br/>
           Expired at: { (this.state.isDeposited > 0) ? (new Date((parseInt(this.state.isDeposited) + parseInt(this.state.expSecs)) * 1000)).toString() : "still no deposite" } <br/>
           FullDeposited at: { (this.state.fullDeposited > 0) ? (new Date(this.state.fullDeposited*1000 )).toString() : "still not"} <br/>
           
           </p>
            }
          </FormGroup>
        </Form>

        <h3>  Make purshaising of option </h3>
        <Form>
          <FormGroup>               

            <p>Current address value is <span className="value font-weight-bold">{this.state.getValue}</span></p>
            <Input type = "number"
                    key="sumToPurshase"
                    step={'.0001'}
                    // initialValues  = {this.state.amountBA1sell}
                        name="sumToPurshase"
                        placeholder="Sum in option tokens you want to buy option"                  
                    onChange={(e) => this.handleChange(e)}/>   

            <FormText >Please approve transfer to option smart contract {this.state.getValue} your sum of {this.state.symbolBA2} tokens {this.state.sumToPurshase * this.state.sd2 * (this.state.amountBA2buy) / ((this.state.amountBA1sell )) /100} to security deposite  exchange for { this.state.sumToPurshase} option. </FormText>
            <br/>
            <Button color="primary" onClick={(e) => this.approveSD(e)}>
              Approve transferFrom {
                    this.state.sumToPurshase * this.state.sd2 * (this.state.amountBA2buy) / ((this.state.amountBA1sell )) /100
                    } of  {this.state.symbolBA2} tokens
            </Button>
            <FormText color="muted">Click the button make approve.</FormText>

              <br/>
            <Button color="primary" onClick={(e) => this.initPurshOpt(e)}>Make purshaising</Button>
            <FormText color="muted">Click the button make purshaising.</FormText>

          </FormGroup>
        </Form>

        <h3> Fulfill option and withdraw  </h3>
        <Form>
        <FormGroup>               
            <p>Current address value is <span className="value font-weight-bold">{this.state.getValue}</span></p>
            <Input type = "number"
                    key="sumToWithdraw"
                    step={'.0001'}
                    // initialValues  = {this.state.amountBA1sell}
                        name="sumToWithdraw"
                        placeholder="Sum in OPTION tokens you want to withdraw"                  
                    onChange={(e) => this.handleChange(e)}/>   

            <FormText >Please first approve transfer to option smart contract {this.state.getValue} the sum of WITHDRAW {this.state.sumToWithdraw} options . 
        
            </FormText>
            <br/>
            <Button color="primary" onClick={(e) => this.approveOpt(e)}>
              Approve to transfer  {this.state.sumToWithdraw} options</Button>
            <FormText color="muted">Click the button make approve.</FormText>

            <FormText >Now approve to transfer the rest part ({ this.state.sumToWithdraw * (100 - this.state.sd2 ) / 100 * web3.utils.toBN(this.state.amountBA2buy).div(web3.utils.toBN(this.state.amountBA1sell ))}) of {this.state.symbolBA2} tokens to fullfill option 

            
            </FormText>
            <br/>
            <Button color="primary" onClick={(e) => this.approveWithdr(e)}>
              Approve fullfilling of option with ({ this.state.sumToWithdraw * (100 - this.state.sd2 ) / 100 * web3.utils.toBN(this.state.amountBA2buy).div(web3.utils.toBN(this.state.amountBA1sell ))}) of {this.state.symbolBA2} tokens</Button>
            <FormText color="muted">Click the button make approve.</FormText>

              <br/>
            <Button color="primary" onClick={(e) => this.finPurshBA2buy(e)}>Make withdraw</Button>
            <FormText color="muted">Click the button make purshaising.</FormText>

            </FormGroup>

            
        </Form>
        <h3> Get penalty and return your deposite </h3>
        <Form>
        <FormGroup>               
            <p>Current address value is <span className="value font-weight-bold">{this.state.getValue}</span></p>
            <Input type = "number"
                    key="sumToWithdraw"
                    step={'.0001'}
                    // initialValues  = {this.state.amountBA1sell}
                        name="sumToWithdraw"
                        placeholder="Sum in OPTION tokens you want to return"                  
                    onChange={(e) => this.handleChange(e)}/>   

            <FormText >Please first approve transfer to option smart contract {this.state.getValue} the sum of options that you have at this wallet address to get penalty {this.state.penalty} . 
        
            </FormText>
            <br/>
            <Button color="primary" onClick={(e) => this.approveOpt(e)}>
              Approve to transfer  {this.state.sumToWithdraw} options</Button>
            <FormText color="muted">Click the button make approve.</FormText>

            <Button color="primary" onClick={(e) => this.getPenalty(e)}>
              Return your deposite and get penalty </Button>
            <FormText color="muted">Click the button make return.</FormText>

            </FormGroup>

            
        </Form>

        <h3> Logs </h3>
        <p> calls being made: </p>
        <div className="logs">
          {
            this.state.logs.map((item, i) => <p key={i}>{item}</p>)
          }
        </div>
      </React.Fragment>
    );
  }
}

export default BuyOption;
