import EmbarkJS from 'Embark/EmbarkJS';
import SimpleStorage from '../../embarkArtifacts/contracts/SimpleStorage';
import MakeOption from '../../embarkArtifacts/contracts/MakeOption';
import React from 'react';
import {Form, FormGroup, Input, HelpBlock, Button, FormText} from 'reactstrap';

class SimpleOptionSDSell extends React.Component, SimpleOption {

  constructor(props) {
    super(props);

    this.state = {
      valueSet: 10,
      valueGet: "",
      logs: [],
      addrBA1: 0x0000000000000000000000,
      addrBA2: 0x0000000000000000000000,
      amountBA1: 1000,
      amountBA2: 1000,
      sd1: 10,
      sd2: 10,
      expDays: 30,
      voidDays: 30,
      description: "",
      optionsList: []  
    };
  }

  handleChange(e) {
    let keyVal = {}
    keyVal[e.target.name] = e.target.value;
    this.setState( keyVal );
                 
  }

  checkEnter(e, func) {
    if (e.key !== 'Enter') {
      return;
    }
    e.preventDefault();
    func.apply(this, [e]);
  }

  async setValue(e) {
    e.preventDefault();
    await EmbarkJS.enableEthereum();
    var value = parseInt(this.state.valueSet, 10);

    SimpleStorage.methods.set(value).send();
    this._addToLog("SimpleStorage.methods.set(value).send()");
  }

  getValue(e) {
    e.preventDefault();

//    SimpleStorage.methods.get().call().then(_value => this.setState({ valueGet: _value }));
    MakeOption.methods.getLast().call().then(_value => this.setState({ valueGet: _value }));

    this._addToLog("SimpleStorage.methods.get(console.log)");
  }

  _addToLog(txt) {
    this.state.logs.push(txt);
    this.setState({ logs: this.state.logs });
  }

  async deployOption (e) {
    
    e.preventDefault();
    await EmbarkJS.enableEthereum();
    MakeOption.methods.makeOption(
                 this.state.addrBA1,
                 this.state.addrBA2,
                 parseInt(this.state.amountBA1, 10),
                 parseInt(this.state.amountBA2, 10),
                 parseInt(this.state.sd1*10, 10),
                 parseInt(this.state.sd2*10, 10),
                 parseInt(this.state.expDays, 10),
                 this.state.description
                ).send();//.then(newaddr => this.setState({ valueGet: newaddr }));

    //ol =  MakeOption.methods.optionList().call(); //.then(oL); // => this.setState({ optionsList: _value }));
    this._addToLog("MakeOption.methods.MakeOption: ", this.state.valueGet);

  }


  render() {
    return (<React.Fragment>
        
        
        <h3> 1. Deploy option    </h3>
          <Form>
                <FormGroup>
                <FormText color="muted">ERC20 token address of your base active (to sell)</FormText>
                  <Input type = "text"
                    key="addrBA1"
                //    value={addrBA1}
                    name="addrBA1"
                    placeholder="Ethereum address 0x0..."                  
                    onChange={(e) => this.handleChange(e)}/>
                    
                 <FormText color="muted">Amount to sell in your tokens</FormText>  
                 <Input type = "number"
                    key="amountBA1"
                    //    value={amountBA1}
                        name="amountBA1"
                        placeholder="Sum in tokens you want to sell"                  
                    onChange={(e) => this.handleChange(e)}/>                  
                  
                 <FormText color="muted"> Percentage You offer for secure deposite  </FormText>  
                 <Input type = "number"
                      key="sd1"
                      //    value={sd1}
                      name="sd1"
                      placeholder="10%"                       
                    onChange={(e) => this.handleChange(e)}/>               

                 <FormText color="muted">ERC20 token address of base active to buy</FormText>
                  <Input type = "text"
                      key="addrBA2"
                 //     value={addrBA2}
                      name="addrBA2"
                      placeholder="Ethereum address 0x0..."  
                    onChange={(e) => this.handleChange(e)}/>
                    
                 <FormText color="muted">Amount to sell in 'their' tokens</FormText>  
                 <Input type = "number"
                     key="amountBA2"
                    //    value={amountBA1}
                    name="amountBA2"
                    placeholder="Sum in tokens you want to buy for your tokens "                  
                
                    onChange={(e) => this.handleChange(e)}/>                  
                  
                 <FormText color="muted"> Percentage You ASK  for secure deposite from buyers </FormText>  
                 
                 <Input type = "number"
                    key="sd2"
                  //    value={sd1}
                    name="sd2"
                    placeholder="10%"                    
                    onChange={(e) => this.handleChange(e)}/>         

                  <FormText color="muted"> Days before mature the option after funded by YOU </FormText>  
                 <Input type = "number"
                    key="expDays"
                    //    value={sd1}
                      name="expDays"
                      placeholder="30"   
                    onChange={(e) => this.handleChange(e)}/>         

                <FormText color="muted"> Days before void the option after matured </FormText>                  
                 <Input type = "number"
                    key="voidDays"
                    //    value={sd1}
                      name="voidDays"
                      placeholder="30"                    
                    onChange={(e) => this.handleChange(e)}/> 
                <FormText color="muted">Description for option </FormText>
                  <Input type = "text"
                      key="description"
                 //     value={addrBA2}
                      name="description"
                      placeholder="description here... "  
                    onChange={(e) => this.handleChange(e)}/>

                <Button color="primary" onClick={(e) => this.deployOption(e)}>Deploy option </Button>


                </FormGroup>

          </Form>
          <h3> 2. Get the current value</h3>
        <Form>
          <FormGroup>
            <Button color="primary" onClick={(e) => this.getValue(e)}>Get Value</Button>
            <FormText color="muted">Click the button to get the current value. The initial value is 100.</FormText>
            {this.state.valueGet && this.state.valueGet !== 0 &&
            <p>Current value is <span className="value font-weight-bold">{this.state.valueGet}</span></p>}
          </FormGroup>
        </Form>

        <h3> 3. Contract Calls </h3>
        <p>Javascript calls being made: </p>
        <div className="logs">
          {
            this.state.logs.map((item, i) => <p key={i}>{item}</p>)
          }
        </div>
      </React.Fragment>
    );
  }
}

export default SimpleOption;
